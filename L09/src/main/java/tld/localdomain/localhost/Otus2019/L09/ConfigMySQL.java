//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-07 14:01:52 korskov>

package tld.localdomain.localhost.Otus2019.L09;

public interface ConfigMySQL {
    String URL = "jdbc:mysql://127.0.0.1:13306/" //
        + "?characterEncoding=utf-8" //
        + "&useUnicode=true" //
        + "&useSSL=false" //
        + "&allowPublicKeyRetrieval=true",
        USR = "dba", PWD = "sql";
}
