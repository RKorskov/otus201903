//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-07 13:43:11 korskov>

package tld.localdomain.localhost.Otus2019.L09;

/**
 * L9 Homemade ORM
 *
 * Напишите JdbcTemplate, который умеет работать с классами, в которых
 * есть поле с аннотацией @Id. JdbcTemplate должен сохранять объект в
 * базу и читать объект из базы. Имя таблицы должно соответствовать
 * имени класса, а поля класса - это колонки в таблице. Проверьте его
 * работу на классе User.
 *
 * Методы JdbcTemplate'а:  
 * ```java
 * void create(T objectData);
 * void update(T objectData);
 * void createOrUpdate(T objectData);
 * <T> T load(long id, Class<T> clazz);
 * ```
 */

interface JdbcTemplate {
 <T> void create(T objectData);
 <T> void update(T objectData);
 <T> void createOrUpdate(T objectData);
 <T> T load(long id, Class<T> clazz);
}
