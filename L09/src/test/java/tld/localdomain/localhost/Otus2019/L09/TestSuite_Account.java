// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-08-08 13:25:28 korskov>

package tld.localdomain.localhost.Otus2019.L09;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
import org.junit.*;
import static org.junit.Assert.*;

import java.util.Objects;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.lang.reflect.Field;
import java.lang.IllegalAccessException;

/**
 * Unit tests for L9 homemade ORM
 *
 * Напишите JdbcTemplate, который умеет работать с классами, в которых
 * есть поле с аннотацией @Id.
 *
 * JdbcTemplate должен сохранять объект в базу и читать объект из
 * базы.
 *
 * Имя таблицы должно соответствовать имени класса, а поля класса -
 * колонки в таблице.
 *
 * Проверьте его работу на классе Account.
 *
 * grant all privileges on `l09ormdb`.* to `dba`@`172.17.%`; flush privileges;
 */

public class TestSuite_Account {
    static final int AID = 17, AREST = 1729, RDIFF = 496;
    static final String ATYPE = "Taxicab H.M. Perfectus";

    private final static String //
        CREATE_DB_ORM = "create database if not exists `L09ORMDB` " //
        + "character set 'utf8'",
        DROP_DB_ORM = "drop database `L09ORMDB`",

        CREATE_TABLE_ACCOUNT = "create table if not exists `L09ORMDB`.`Account` " //
        + "(`no` bigint(20) NOT NULL auto_increment, " //
        + "`type` varchar(255), " //
        + "`rest` decimal(16,2), " //
        + "primary key (`no`))",
        CLEAN_TABLE_ACCOUNT = "delete from `L09ORMDB`.`Account`",
        DROP_TABLE_ACCOUNT = "drop table `L09ORMDB`.`Account`";

    private static void evalSQL(final Connection dbc, final String query)
        throws SQLException {
        try (PreparedStatement pds = //
             dbc.prepareStatement(query)) {
            pds.execute();
        }
    }

    @BeforeClass
    public static void prepareDB() throws SQLException {
        try (Connection dbc = DriverManager.getConnection //
             (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
            evalSQL(dbc, CREATE_DB_ORM);
            evalSQL(dbc, CREATE_TABLE_ACCOUNT);
        }
    }

    @AfterClass
    public static void cleanDB() throws SQLException {
        try (Connection dbc = DriverManager.getConnection //
             (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
            evalSQL(dbc, DROP_TABLE_ACCOUNT);
            evalSQL(dbc, DROP_DB_ORM);
        }
    }

    @After
    public void cleanTables() throws SQLException {
        try (Connection dbc = DriverManager.getConnection //
             (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
            evalSQL(dbc, CLEAN_TABLE_ACCOUNT);
        }
    }

    /**
     * test case:
     * create, load, compare
     */
    @Test
    public void test_createLoad() throws SQLException {
        Account xin, accn = new Account(AID, ATYPE, AREST);
        xin = null; // load();
        JdbcTemplateSQL jt = new JdbcTemplateSQL();
        jt.create(accn);
        jt = null;
        jt = new JdbcTemplateSQL();
        xin = jt.load(AID, Account.class);
        jt = null;
        assertTrue(accn.equals(xin));
    }

    /**
     * test case:
     * create, update, load, compare
     */
    @Test
    public void test_updateLoad() throws SQLException {
        Account xin, accn = new Account(AID, ATYPE, AREST);
        xin = null; // load();
        JdbcTemplateSQL jt = new JdbcTemplateSQL();
        jt.create(accn);
        jt = null;
        accn.add(RDIFF);
        jt = new JdbcTemplateSQL();
        jt.update(accn);
        jt = null;
        jt = new JdbcTemplateSQL();
        xin = jt.load(AID, Account.class);
        jt = null;
        assertTrue(accn.equals(xin));
    }

    /**
     * test case:
     * createOrUpdate, load, compare
     */
    @Test
    public void test_createUpdateLoad() throws SQLException {
        Account xin, accn = new Account(AID, ATYPE, AREST);
        xin = null; // load();
        JdbcTemplateSQL jt = new JdbcTemplateSQL();
        jt.createOrUpdate(accn);
        jt = null;
        jt = new JdbcTemplateSQL();
        xin = jt.load(AID, Account.class);
        jt = null;
        assertTrue(accn.equals(xin));
    }

    /**
     * test case:
     * create, createOrUpdate, load, compare
     */
    @Test
    public void test_recreateUpdateLoad() throws SQLException {
        Account xin, accn = new Account(AID, ATYPE, AREST);
        xin = null; // load();
        JdbcTemplateSQL jt = new JdbcTemplateSQL();
        jt.create(accn);
        jt = null;
        accn.add(RDIFF);
        jt = new JdbcTemplateSQL();
        jt.createOrUpdate(accn);
        jt = null;
        jt = new JdbcTemplateSQL();
        xin = jt.load(AID, Account.class);
        jt = null;
        assertTrue(accn.equals(xin));
    }
}
