-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-08-19 17:06:02 korskov>

Overview
========

otus-java_2019-03

Группа 2019-03  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov)  
korskov.r.v@gmail.com  

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

Домашние Работы
===============

# L9 Homemade ORM #

Работа должна использовать базу данных H2.  
Создайте в базе таблицу User с полями:
1. id bigint(20) NOT NULL auto_increment
2. name varchar(255)
3. age int(3)

Создайте свою аннотацию @Id .

Создайте класс User (с полями, которые соответствуют таблице, поле id отметьте аннотацией).

Напишите JdbcTemplate, который умеет работать с классами, в которых
есть поле с аннотацией @Id. JdbcTemplate должен сохранять объект в
базу и читать объект из базы. Имя таблицы должно соответствовать имени
класса, а поля класса - это колонки в таблице. Проверьте его работу на
классе User.

Методы JdbcTemplate'а:  
```java
void create(T objectData);
void update(T objectData);
void createOrUpdate(T objectData); // опционально.
<T> T load(long id, Class<T> clazz);
```

Метод createOrUpdate - необязательный.
Он должен "проверять" наличие объекта в таблице и создавать новый или обновлять.

Создайте еще одну таблицу Account:
1. no bigint(20) NOT NULL auto_increment
2. type varchar(255)
3. rest number

Создайте для этой таблицы класс Account и проверьте работу JdbcTemplate на этом классе.

Еще одна опция (по желанию):
Реализовать абстракцию DBService для объектов User и Account.

И еще одна опция (по желанию для супер-мега крутых бизонов):
прикрутите этот "jdbc-фреймворк" к департаменту ATM.
