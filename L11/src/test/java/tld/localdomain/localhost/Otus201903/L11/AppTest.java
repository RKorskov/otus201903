//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-09-18 22:01:08 roukoru>

package tld.localdomain.localhost.Otus201903.L11;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
import org.junit.runners.Suite;

@Suite.SuiteClasses({
    TestSuite_Smoke.class,
})

public class AppTest {}
