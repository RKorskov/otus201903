//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-09-28 15:29:25 roukoru>

package tld.localdomain.localhost.Otus201903.L11.dbservice;

/**
 * Добавьте кэширование в DBService из задания про Hibernate ORM
 */

import tld.localdomain.localhost.Otus201903.L10.User;
import tld.localdomain.localhost.Otus201903.L10.UserDAO;
import tld.localdomain.localhost.Otus201903.L11.ucache.UCache;
import tld.localdomain.localhost.Otus201903.L11.ucache.UCachEngine;

public class DBService {
    private UserDAO userDAO = null;
    private UCache uCache = null;

    public DBService() {
        initDao();
        initCache();
    }
    private void initDao() {userDAO = new UserDAO();}
    private void initCache() {uCache = UCachEngine.create();}

    public DBService(UCache uCache, UserDAO userDAO) {
        this.uCache = uCache;
        this.userDAO = userDAO;
    }

    public User get(final long uid) {
        User obj = null;
        if(uCache == null)
            initCache();
        obj = uCache.get(uid);
        if(obj != null)
            return obj;
        if(userDAO == null)
            initDao();
        obj = userDAO.load(uid);
        uCache.put(uid, obj);
        return obj;
    }

    public long put(final User obj) {
        if(userDAO == null)
            initDao();
        if(uCache == null)
            initCache();
        /*
        final long uid = obj.getId();
        User t = uCache.get(uid);
        if(t != null)
            uCache.refresh(uid);
         */
        uCache.put(obj.getId(), obj);
        boolean rf = userDAO.store(obj);
        return obj.getId();
    }
}

