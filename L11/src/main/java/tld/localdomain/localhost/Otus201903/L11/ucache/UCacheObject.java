//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-09-28 15:26:37 roukoru>

package tld.localdomain.localhost.Otus201903.L11.ucache;

import tld.localdomain.localhost.Otus201903.L10.User;

import java.io.Serializable;

/**
 * user cached tuple: user (per se), age of this cache object
 */
class UCacheObject<U> implements Serializable {
    private final U user;
    private long age;

    UCacheObject(final U user) {
        this.user = user;
        age = -1;
    }

    //public static UCacheObject create(final U user) {return new UCacheObject(user);}
    public void setAge(long n) {age = n;}
    public long getAge() {return age;}
    public U get() {return user;}
}
