//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-09-28 15:19:19 roukoru>

package tld.localdomain.localhost.Otus201903.L11.ucache;

/**
 * Добавьте кэширование в DBService из задания про Hibernate ORM
 */

import tld.localdomain.localhost.Otus201903.L10.User;

import java.util.function.Function;

public interface UCache {
    long put(long uid, User user);
    User get(long uid);
    User get(long uid, Function<Long, User> getter); // set default getter
    // User has(long uid); // ]
    // long has(User user);
    void refresh(long uid); // ?
    int getCountHit();
    int getCountMiss();
}
