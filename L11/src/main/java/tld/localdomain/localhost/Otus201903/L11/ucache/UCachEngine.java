//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-09-28 15:18:48 roukoru>

package tld.localdomain.localhost.Otus201903.L11.ucache;

import tld.localdomain.localhost.Otus201903.L10.User;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Добавьте кэширование в DBService из задания про Hibernate ORM
 */
public class UCachEngine implements UCache {
    private final Map <Long, SoftReference<UCacheObject<User>>> cache; // Queue?

    private UCachEngine() { cache = new HashMap<>(); }
    public static UCache create() {return new UCachEngine();}

    public long put(final long uid, final User user) {
        cache.put(uid, new SoftReference<>(new UCacheObject<>(user)));
        return uid;}
    public User get(final long uid) throws NullPointerException {return cache.get(uid).get().get();}
    public User get(long uid, Function<Long, User> getter) {return null;} // DBService.get
    // public User has(final long uid) {return null;}
    // public long has(final User user) {return 0;}
    public void refresh(final long uid) {
        try {
            cache.get(uid).get().setAge(0);
        } catch (NullPointerException ignored) {}
    }
    public int getCountHit() {return 0;}
    public int getCountMiss() {return 0;}
}
