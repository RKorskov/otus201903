-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-08-22 19:42:27 korskov>

Overview
========

otus-java_2019-03

Группа 2019-03  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov)  
korskov.r.v@gmail.com  

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

Домашние Работы
===============

# L11 Homemade cache engine #

Напишите свой cache engine с soft references.  
Добавьте кэширование в DBService из задания про Hibernate ORM

