// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-08-05 15:49:00 korskov>

package tld.localdomain.localhost.Otus201903;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
// import org.junit.runner.RunWith;
import org.junit.runners.Suite;

// @RunWith(Suite.class)

// TestSuite_Executor.class
@Suite.SuiteClasses({
   TestSuite_Smoke.class
})

public class AppTest {}
