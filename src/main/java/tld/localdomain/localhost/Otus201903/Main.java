// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-08-05 15:49:00 korskov>
//
package tld.localdomain.localhost.Otus201903;

/**
 * 2019-08
 * @author RKorskov
 */
public class Main {
    public static void main(final String[] args) {
        System.out.println("Hello World!");
    }
}
