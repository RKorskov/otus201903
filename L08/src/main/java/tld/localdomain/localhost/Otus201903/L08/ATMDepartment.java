// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-29 13:27:15 korskov>

package tld.localdomain.localhost.Otus201903.L08;
import tld.localdomain.localhost.Otus201903.L07.*;

/**
 * <h3>L8 ATM Department</h3>
 * <p>
 * Написать приложение ATM Department:
 * <ol>
 * <li> Приложение может содержать несколько ATM;
 * <li> Departmant может собирать сумму остатков со всех ATM;
 * <li> Department может инициировать событие – восстановить состояние
 * всех ATM до начального (начальные состояния у разных ATM могут быть
 * разными);
 * </ol>
 */

import java.util.ArrayList;

public class ATMDepartment {
    private ArrayList <ATM> atms;

    private ATMDepartment(){
        atms = null;
    }

    public static ATMDepartment createATMDepartment() {
        return new ATMDepartment();
    }

    public boolean addATM(ATM atm) {
        if(atms == null)
            atms = new ArrayList<>();
        atms.add(atm);
        return true;
    }

    /**
     * Department может инициировать событие – восстановить состояние
     * всех ATM до начального (начальные состояния у разных ATM могут быть
     * разными);
     */
    public void resetAll() {
        atms.forEach(ATM::reset);
        return;
    }

    /**
     * Departmant может собирать сумму остатков со всех ATM;
     * @return сумма остатков
     */
    public long collectLeftoversAll() {
        long ra = 0;
        for(ATM atm : atms) {
            final int t = atm.getTotal(); // getAllAndSet0 ?
            atm.get(t); // flush cash
            ra += t;
        }
        return ra;
    }
}
