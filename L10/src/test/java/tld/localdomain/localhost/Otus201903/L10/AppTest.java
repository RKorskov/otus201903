//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-26 13:10:42 korskov>

package tld.localdomain.localhost.Otus201903.L10;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
// import org.junit.runner.RunWith;
import org.junit.runners.Suite;

// @RunWith(Suite.class)

// TestSuite_Executor.class
@Suite.SuiteClasses({
        TestSuite_Smoke.class,
})

public class AppTest {}
