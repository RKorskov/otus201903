// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-08-30 15:38:45 korskov>

package tld.localdomain.localhost.Otus201903.L10;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
import org.junit.*;
import static org.junit.Assert.*;

import java.util.Objects;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.lang.reflect.Field;
import java.lang.IllegalAccessException;

/**
 * Unit tests for L10
 */

public class TestSuite_Smoke {
    private final static int UID = 41, UAGE = 43;
    private final static String UNAME = "A.Dent",
        UADDR = "Interstate 60, b.41",
        UPHN0 = "1-23-456-7890",
        UPHN1 = "2345-678-90-1";

    /*
    @BeforeClass
    public static void prepareDB() throws SQLException {
        try (Connection dbc = DriverManager.getConnection //
             (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
            evalSQL(dbc, CREATE_DB_ORM);
            evalSQL(dbc, CREATE_TABLE_USER);
            evalSQL(dbc, CREATE_TABLE_ACCOUNT);
        }
    }

    @AfterClass
    public static void cleanDB() throws SQLException {
        try (Connection dbc = DriverManager.getConnection //
             (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
            evalSQL(dbc, DROP_TABLE_USER);
            evalSQL(dbc, DROP_TABLE_ACCOUNT);
            evalSQL(dbc, DROP_DB_ORM);
        }
    }

    @After
    public void cleanTables() throws SQLException {
        try (Connection dbc = DriverManager.getConnection //
             (ConfigMySQL.URL, ConfigMySQL.USR, ConfigMySQL.PWD)) {
            evalSQL(dbc, CLEAN_TABLE_USER);
            evalSQL(dbc, CLEAN_TABLE_ACCOUNT);
        }
    }
    */

    /**
     * test case:
     * create, load, compare
     */
    @Test
    public void test_storeLoad() {
        User xiao, lao = new User(UNAME, UAGE);
        lao.setAddress(UADDR);
        lao.setPhone(UPHN0);
        lao.setPhone(UPHN1);
        UserDAO hsl = new UserDAO();
        boolean f = hsl.store(lao);
        xiao = hsl.load(lao.getId());
        assertTrue(lao.equals(xiao));
    }

}
