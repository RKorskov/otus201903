//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-30 15:37:30 korskov>

package tld.localdomain.localhost.Otus201903.L10;

/**
 * L10 Hibernate
 *
 * Добавьте в User поля:
 * 1. адрес (OneToOne)
 *  `class AddressDataSet {private String street;}`;
 * 2. телефон (OneToMany)
 *  `class PhoneDataSet {private String number;}`;
 *
 * Разметьте классы таким образом, чтобы при сохранении/чтении объекта
 * User каскадно сохранялись/читались вложенные объекты.
 */

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;

public class User implements Serializable {
    private long id;
    private String name;
    // private byte age;
    private int age;
    private AddressDataSet address;
    private List<PhoneDataSet> phone;

    User(){;}

    User(long id, String name, int age) {
        this.id = id;
        this.name = name;
        // this.age = (byte)age;
        this.age = age;
        this.address = null;
        this.phone = new ArrayList <PhoneDataSet>();
    }

    public User(String name, int age) {
        this.name = name;
        this.age = (byte)age;
        this.address = null;
        this.phone = new ArrayList <PhoneDataSet>();
    }

    public long getId() {return id;}
    public void setId(final long n) {id=n;}
    public String getName() {return name;}
    public void setName(final String s) {name = s;}
    public int getAge() {return age;}
    // public void setAge(final int n) {age = (byte)n;}
    public void setAge(final int n) {age = n;}
    // public void setAge(final byte n) {age = n;}

    public AddressDataSet getAddress() {return address;}
    public void setAddress(final String s) {address = new AddressDataSet(s);}
    public void setAddress(final AddressDataSet address) {this.address = address;}

    public List <PhoneDataSet> getPhone() {return phone;}
    public void setPhone(final String s) {
        if(phone == null)
            phone = new ArrayList <PhoneDataSet>();
        phone.add(new PhoneDataSet(s));
    }
    public void setPhone(final List <PhoneDataSet> phone) {this.phone = phone;}

    @Override
    public boolean equals(final Object obj) {
        if(obj == this)
            return true;
        if(obj == null || getClass() != obj.getClass())
            return false;
        User uobj = (User) obj;
        return uobj.id == id //
            && uobj.age == age //
            && Objects.equals(name, uobj.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age);
    }

    @Override
    public String toString() {
        return String.format("id=%d  name='%s'  age=%d", id, name, age);
    }
}
