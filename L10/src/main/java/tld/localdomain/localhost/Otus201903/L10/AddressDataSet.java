//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-30 15:40:16 korskov>

package tld.localdomain.localhost.Otus201903.L10;

/**
 * L10 Hibernate
 *
 * 1. адрес (OneToOne)
 *  `class AddressDataSet {private String street;}`;
 *
 * Разметьте классы таким образом, чтобы при сохранении/чтении объекта
 * User каскадно сохранялись/читались вложенные объекты.
 */

import java.util.Objects;

public class AddressDataSet {
    private long id;
    private String street;
    AddressDataSet(){}
    AddressDataSet(String s) {street = s;}

    public String getStreet() {return street;}
    public void setStreet(final String v) {street = v;}
    public long getId() {return id;}
    public void setId(final long n) {id = n;}
    public String toString() {return street;}
}
