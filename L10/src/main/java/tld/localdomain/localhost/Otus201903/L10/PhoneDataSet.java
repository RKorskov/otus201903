//-*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
//-*- eval: (set-language-environment Russian) -*-  
//--- Time-stamp: <2019-08-30 15:40:35 korskov>

package tld.localdomain.localhost.Otus201903.L10;

/**
 * L10 Hibernate
 *
 * 2. телефон (OneToMany)
 *  `class PhoneDataSet {private String number;}`;
 *
 * Разметьте классы таким образом, чтобы при сохранении/чтении объекта
 * User каскадно сохранялись/читались вложенные объекты.
 */

import java.util.Objects;

public class PhoneDataSet {
    private long id;
    private String number;
    PhoneDataSet(){}
    PhoneDataSet(String s) {number = s;}

    public String getNumber() {return number;}
    public void setNumber(final String v) {number = v;}
    public long getId() {return id;}
    public void setId(final long n) {id = n;}
    public String toString() {return number;}
}
