// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-08-30 15:49:14 korskov>

package tld.localdomain.localhost.Otus201903.L10;

import java.util.List;
import javax.persistence.TypedQuery;

import org.hibernate.*;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * Store and load User's instance(s)
 */
public class UserDAO {
    private static final String HCFG = "hibernate.cfg.xml";

    public boolean store(final Object obj) {
        if(!(obj instanceof User))
            return false;
        final User user = (User)obj;
        Session hss = null;
        Transaction htr = null;
        try {
            StandardServiceRegistry ssr = //
                new StandardServiceRegistryBuilder().configure(HCFG).build();
            Metadata meta = //
                new MetadataSources(ssr).getMetadataBuilder().build();
            SessionFactory factory = meta.getSessionFactoryBuilder().build();
            hss = factory.openSession();
            htr = hss.beginTransaction();
            hss.persist(user); // ???
            //hss.save(user);
            htr.commit();
            return true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        finally {
            //if(htr != null)
            if(hss != null) {
                //hss.getTransaction().commit();
                hss.close();
            }
        }
    }

    public User load(final long id) {
        Session hss = null;
        try {
            StandardServiceRegistry ssr = //
                new StandardServiceRegistryBuilder().configure(HCFG).build();
            Metadata meta = //
                new MetadataSources(ssr).getMetadataBuilder().build();
            SessionFactory factory = meta.getSessionFactoryBuilder().build();
            hss = factory.openSession();
            TypedQuery query = hss.createQuery("from User u where u.id = :id");
            query.setParameter("id", id);
            for(User u : (List <User>) query.getResultList()) {
                if(u.getId() == id) {
                    return u;
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            if(hss != null)
                hss.close();
        }
        return null;
    }
}
