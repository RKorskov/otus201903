-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-08-30 14:56:21 korskov>

Overview
========

otus-java_2019-03

Группа 2019-03  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov)  
korskov.r.v@gmail.com  

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

Домашние Работы
===============

# L10 Использование Hibernate #

Возьмите за основу предыдущее ДЗ (Самодельный ORM) и реализуйте
функционал сохранения и чтения объекта User через Hibernate (рефлексия
больше не нужна). Конфигурация Hibernate должна быть вынесена в файл.

Добавьте в User поля:  
1. адрес (OneToOne)
   `class AddressDataSet {private String street;}`;
2. телефон (OneToMany)
   `class PhoneDataSet {private String number;}`;

Разметьте классы таким образом, чтобы при сохранении/чтении объекта
User каскадно сохранялись/читались вложенные объекты.

2Read
=====

[Basic Hibernate Example with XML Configuration 2015-06](https://www.onlinetutorialspoint.com/hibernate/hibernate-example.html)
