// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-08-01 17:48:39 korskov>

package tld.localdomain.localhost.Otus201903.L07;

// JUnit 4.x
import org.junit.*;
import static org.junit.Assert.*;

public class TestSuite_CashBox {
    private final static int __NOMINAL = 7;

    @Test
    public void createNew_empty() {
        CashBox cb = new SimpleCashBox();
        assertTrue(true);
    }

    @Test
    public void createNew_filled() {
        BankNote bns[] = new BankNote[1];
        bns[0] = new BankNote(3,4);
        CashBox cb = new SimpleCashBox(bns);
        assertTrue(true);
    }

    @Test
    public void test_put() {
        CashBox cb = new SimpleCashBox();
        // BankNote[] bnIn = new BankNote[], bnOut = new BankNote[];
        assertTrue(true);
    }

    @Test
    public void test_get() {
        CashBox cb = new SimpleCashBox();
        assertTrue(true);
    }

    @Test
    public void test_getTotal() {
        CashBox cb = new SimpleCashBox();
        assertTrue(true);
    }
}
