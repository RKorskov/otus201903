package tld.localdomain.localhost.Otus201903.L07;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
    TestSuite_BankNote.class,
    TestSuite_CashBox.class,
    TestSuite_ATM.class
})

public class AppTest {}
