// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-31 18:20:39 korskov>

package tld.localdomain.localhost.Otus201903.L07;

/**
 * кассета для банкнот для ATM
 */
public interface CashBox {
    /**
     * извлекает запрошенную сумму из кассеты;
     * возвращает null при невозможности выдать запрошенную сумму;
     */
    public BankNote[] get(int amount);

    /**
     * помещает данную сумму в кассету
     */
    public boolean put(BankNote[] cash);

    /**
     * возвращает общую сумму в кассете
     */
    public int getTotal();
}
