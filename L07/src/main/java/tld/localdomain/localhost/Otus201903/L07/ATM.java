// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-09-24 13:56:28 korskov>

package tld.localdomain.localhost.Otus201903.L07;

//import Otus2019.L06.CashPile;

/**
 * L7 Написать эмулятор АТМ (банкомата)
 *
 * Объект класса АТМ должен уметь:
 * 1. принимать банкноты разных номиналов (на каждый номинал должна быть
 *    своя ячейка);
 * 2. выдавать запрошенную сумму минимальным количеством банкнот или
 *    ошибку если сумму нельзя выдать;
 * 3. выдавать сумму остатка денежных средств;
 * @author RKorskov
 */

public interface ATM {
    /**
     * помещает данную сумму в (кассету) ATM
     */
    boolean put(BankNote[] cash);

    /**
     * извлекает запрошенную сумму из (кассеты) ATM;
     * возвращает null при невозможности выдать запрошенную сумму;
     */
    BankNote[] get(int amount);

    /**
     * возвращает общую сумму в (кассете) ATM
     */
    int getTotal();

    /**
     * (ATMDepartment)
     * сброс состояния ATM до начального
     */
    boolean reset();
}
